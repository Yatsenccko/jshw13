const images = document.querySelectorAll('.image-to-show')
let currentImageIndex = 0
function fadeOut(current) {
    images[current].classList.remove('active')
}
function fadeIn(current) {
    images[current].classList.add('active')
}
function next() {
    fadeOut(currentImageIndex)
    currentImageIndex++
    if(currentImageIndex >= images.length || currentImageIndex < 0){
        currentImageIndex = 0
    }
    fadeIn(currentImageIndex)
}
let time = setInterval(next, 3000)
const btnStop = document.querySelector('.btnStop')
const btnStart = document.querySelector('.btnStart')
btnStop.addEventListener('click',()=>{
    clearInterval(time)
})
btnStart.addEventListener('click',()=>{
    time = setInterval(next, 3000)
})